FROM node

RUN mkdir /app
WORKDIR /app

COPY package.json /app

COPY . /app
RUN yarn install && yarn test
RUN yarn build

CMD yarn start #only docker run

EXPOSE 3000

